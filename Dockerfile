FROM debian:sid-slim

RUN apt-get update && \
	apt-get install -y --no-install-recommends pandoc texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended texlive-lang-german #texlive-fonts-extra texlive-lang-all \
	&& rm -rf /var/lib/apt/lists/*

COPY handwritten.sh /usr/local/bin/handwritten
RUN chmod +x /usr/local/bin/handwritten

RUN mkdir -p $HOME/.pandoc
COPY templates $HOME/.pandoc/templates

WORKDIR /app

CMD ["handwritten"]
