IMAGE = registry.gitlab.com/space-essentials/handwritten

.PHONY: image
image:
	docker build -t ${IMAGE} .

.PHONY: shell
shell:
	docker run -it --rm -v $$PWD:/app ${IMAGE} bash

.PHONY: test
test:
	make image && cd example && rm -f output/*.pdf && make && xdg-open output/test.pdf
