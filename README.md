# Handwritten

Tooling to create the necessary donation receipts for the good old german bureaucracy (Spendenquittungen).

## Usage

* install dependencies `apt install make docker`
* see [example folder](example) or download [last release](https://gitlab.com/space-essentials/handwritten/-/jobs/artifacts/master/download?job=dist)
* enter your details in `config.yml`
* copy the example `sachzuwendung.md` (options from the config can be overwritten here)
* run `make`

## Development

* run `make test`

# Acknowledgement

* [latex templates thx to Uwe Ziegenhagen](https://github.com/UweZiegenhagen/spendenquittungen-mit-latex)
